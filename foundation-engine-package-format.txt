FORMAT DOCUMENTATION
====================

All numeric values are in little-endian.

Overall division:
  [file header]   [index]           [data section]
   12 bytes        variable length   variable length

File header:
  [data section offset]   [?? unknown]   [?? unknown]
   4 bytes                 4 bytes        4 bytes

Index (starts at 0xC, ends at [data section offset] exclusive):
  array of:
    [filename length]   [filename]         [is directory]      [... variable header]
     4 bytes             variable length    1 byte (boolean)    8 bytes

Variable header for directory:
  [item count]   [ends at offset]
   4 bytes        4 bytes

Variable header for file:
  [start offset]   [data length]
   4 bytes          4 bytes

NOTE: The [start offset] is relative to the [data section offset]!

Data section is purely non-delimited data, accessible by byte offset from the index.